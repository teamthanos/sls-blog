# README #

### What is this repository for? ###

* a Serverless plugin for the sls-cms application. Like SLS-CMS, SLS-Blog will create static files from the content, served initially from S3 and growing to support other types as they are added into the serverless framework
* 0.0.1a

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Dave LeBlanc